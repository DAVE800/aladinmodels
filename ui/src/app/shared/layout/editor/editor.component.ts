import { Component, Input, OnInit } from '@angular/core';
import { ColorEvent } from 'ngx-color';

import { fabric } from 'fabric';
import { EditorserviceService } from 'src/app/core';
import { BodyComponent } from '../body/body.component';
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
@Input() data:any;
width=400;
height=400
canvas:any;
iH=400;
iW=400
hastext=false;
canvaswrapper:any
text:any;
testingpicture:any
cpt=0;
shapes=[
  {
    url:"/assets/shape1.png"
  },

  {
    url:"/assets/shape2.png"
  },
  {
    url:"/assets/shape3.png"
  },

  {
    url:"/assets/shape4.jpg"
  },
  {
    url:"/assets/shape8.png"
  },
  {
    url:"/assets/shape7.jpeg"
  },
  {
    url:"/assets/shape9.jpg"
  },
  {
    url:"/assets/shape10.jpeg"
  },

]

testcanvas:any;
colors=["blue","white","black","red","orange","yellow","green","#999999","#454545","#800080","#000080","#00FF00","#800000","#008080"]
  constructor(private Editor:EditorserviceService) { }

  ngOnInit(): void {

    this.testcanvas= new fabric.Canvas('test',{
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      stopContextMenu:false

    });




  this.canvas= new fabric.Canvas('canvas-wrapper',{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false

  });



  this.canvas.filterBackend=new fabric.WebglFilterBackend();



  var canvasWrapper:any = document.getElementById('wrapper');
// initial dimensions
canvasWrapper.style.width = this.iW;
canvasWrapper.style.height = this.iH;

setInterval(() => {
  const newWidth = canvasWrapper.clientWidth
  const newHeight = canvasWrapper.clientHeight
  if (newWidth !== this.iW || newHeight !== this.iH) {
    this.width = newWidth
    this.height = newHeight
    this.iW=this.width;
    this.iH=this.height
    this.canvas.setWidth(newWidth)
    this.canvas.setHeight(newHeight)
  }
}, 100)


  this.canvas.setWidth(this.width);
  this.canvas.setHeight(this.height);

  this.Editor.MakeImage(this.data.url,this.canvas);




}


  makeItalic(){
    this.Editor.italic(this.canvas)

  }

  makeBold(){
    this.Editor.bold(this.canvas)
  }

  underlineText(){
    this.Editor.underline(this.canvas)
  }


  overlineText(){
    this.Editor.overline(this.canvas)
  }


  addText(){
    if(!this.hastext){
      this.Editor.addText(this.canvas);

      this.hastext=true;
    }
  }


  copy(){
    this.Editor.copy(this.canvas)
  }

  paste(){
    this.Editor.paste(this.canvas)
  }


  removeItem(){

    this.Editor.remove(this.canvas);
  }

  InputChange(Inputtext:any){
    if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){
      if(this.cpt==0){
        this.text=this.canvas.getActiveObject().text+" "+ this.text
        this.cpt=this.cpt+1
        this.canvas.getActiveObject().text= this.text
        this.canvas.requestRenderAll();
      }else{
        this.canvas.getActiveObject().text= this.text
        this.canvas.requestRenderAll();
      }

    }else{

      let text= new fabric.Textbox(this.text,{
        top:200,
        left:200,
        fill:"blue",
        fontSize:38,
        fontStyle:'normal',
        cornerStyle:'circle',
        selectable:true,
        borderScaleFactor:1,
        overline:false,
        lineHeight:1.5
      });

      this.canvas.add(text).setActiveObject(text);
      this.canvas.renderAll(text);
      this.canvas.requestRenderAll();
      this.canvas.centerObject(text);
    }


  }

  texteclor($event:ColorEvent){
    this.Editor.textcolor($event.color.hex,this.canvas);

  }



  setItem(event:any){
  this.Editor.setitem(event,this.canvas)
  }


  onFileUpload(event:any){
    let file = event.target.files[0];
    if(!this.Editor.handleChanges(file)){

      const reader = new FileReader();

    reader.onload = () => {
      let url:any = reader.result;
      fabric.Image.fromURL(url,(oImg) =>{
      oImg.set({
          scaleX:0.5,
          scaleY:0.5,
          crossOrigin: "Anonymous",
    });
      this.canvas.add(oImg).setActiveObject(oImg);
      this.canvas.centerObject(oImg);
      this.canvas.renderAll(oImg)

    })
      };
      reader.readAsDataURL(file);

    }

  }


  resize(){
    this.canvas.setWidth(this.width);
    this.canvas.setHeight(this.height);
    this.canvas.centeredScaling=true
     this.canvas.renderAll()

  }


  InputSize(){
    var canvasWrapper:any = document.getElementById('wrapper');
    canvasWrapper.style.width = this.width;
canvasWrapper.style.height = this.height;
this.canvas.setWidth(this.width);
this.canvas.setHeight(this.width);
  }
// initial dimensions



  getModel(){
    for(let item of this.canvas.getObjects()){
     item.set({lockMovementX:true})
     item.set({lockMovementY:true})
     item.set({lockScalingY:true})
     item.set({lockScalingX:true})



    // item.set({selectable:false})


    }
    this.testingpicture=this.canvas.toDataURL()
    var json = this.canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.testcanvas.loadFromJSON(json, this.testcanvas.renderAll.bind(this.testcanvas));
    this.testcanvas.setHeight(this.height)
    this.testcanvas.setWidth(this.width)


  }

}
