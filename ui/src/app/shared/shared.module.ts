import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent,HeaderComponent  } from './layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselComponent } from './layout/carousel/carousel.component';
import { BodyComponent } from './layout/body/body.component';
import { ProductsComponent } from './layout/products/products.component';
import { EditorComponent } from './layout/editor/editor.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ChatbotComponent } from './layout/chatbot/chatbot.component';
import { FormsModule } from '@angular/forms';
import { ColorCircleModule } from 'ngx-color/circle'; 


@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    CarouselComponent,
    BodyComponent,
    ProductsComponent,
    EditorComponent,
    SidebarComponent,
    ChatbotComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ColorCircleModule 

  ],
  exports:[
    FooterComponent,
    HeaderComponent,
    CommonModule,
    NgbModule,
    CarouselComponent,
    BodyComponent,
    ProductsComponent,
    EditorComponent,
    FormsModule,
    ColorCircleModule 


  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
